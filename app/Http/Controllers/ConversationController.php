<?php

namespace App\Http\Controllers;

use App\Conversation;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    public function index() {
        return Conversation::where('user_id', auth()->id())->get([
            'id',
            'last_message',
            'last_date',
            'contact_id',
            'is_blocked',
            'has_notifications',
        ]);
    }
}

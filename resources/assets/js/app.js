require('./bootstrap');
window.Vue = require('vue');
// noinspection JSAnnotator
import BootstrapVue from 'bootstrap-vue';


Vue.use(BootstrapVue);

Vue.component('contact-component', require('./components/ContactComponent.vue'));
Vue.component('contact-list-component', require('./components/ContactListComponent.vue'));
Vue.component('active-chat-component', require('./components/ActiveChatComponent.vue'));
Vue.component('message-conversation-component', require('./components/MessageConversationComponent.vue'));
Vue.component('messenger-component', require('./components/MessengerComponent.vue'));
Vue.component('status-component', require('./components/StatusComponent.vue'));
Vue.component('profile-form-component', require('./components/ProfileFormComponent.vue'));

const app = new Vue({
    el: '#app',
    methods: {
        logout() {
            document.getElementById('logout-form').submit();
        }
    }
});

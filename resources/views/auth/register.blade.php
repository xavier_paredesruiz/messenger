@extends('layouts.app')

@section('content')
<b-container>
    <b-row align-h="center">
        <b-col cols="8" >
            <b-card title="Registro">

                @if ($errors->any())
                    @foreach($errors->all() as $error)
                        <b-alert variant="danger" show>
                            {{ $error }}
                        </b-alert>
                    @endforeach
                @endif

                <b-form method="POST" action="{{ route('register') }}" aria-label="{{ __('Registro') }}">
                    @csrf

                    <b-form-group id="nameGroup"
                                  label="Nombre:"
                                  label-for="name">
                        <b-form-input id="name"
                                      type="text"
                                      name="name"
                                      required
                                      autofocus
                                      value="{{ old('name') }}"
                                      placeholder="p.e. John Doe">
                        </b-form-input>
                    </b-form-group>

                    <b-form-group id="emailGroup"
                                  label="Correo electrónico:"
                                  description="No compartiremos tu correo electrónico con terceros."
                                  label-for="email">
                        <b-form-input id="email"
                                      type="email"
                                      name="email"
                                      required
                                      value="{{ old('email') }}"
                                      placeholder="p.e. example@email.com">
                        </b-form-input>
                    </b-form-group>

                    <b-form-group id="passwordGroup"
                                     label="Contraseña:"
                                     label-for="password">
                        <b-form-input id="password"
                                      type="password"
                                      name="password"
                                      required>
                        </b-form-input>
                    </b-form-group>

                    <b-form-group id="passwordConfirmGroup"
                                  label="Confirmar contraseña:"
                                  label-for="password_confirmation">
                        <b-form-input id="password_confirmation"
                                      name="password_confirmation"
                                      type="password"
                                      required>
                        </b-form-input>
                    </b-form-group>

                    <b-button type="submit" variant="primary">
                        Registro
                    </b-button>

                    <b-button href="{{ route('login') }}" variant="link">
                        ¿Ya te has registrado? Inicia sesión aquí.
                    </b-button>

                </b-form>
            </b-card>
        </b-col>
    </b-row>
</b-container>
@endsection

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit() {
        return view('profile');
    }

    public function update(Request $request) {

        $user = auth()->user();
        $user->name = $request->name;
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        $file = $request->avatar;

        if ($file) {
            $path = public_path('/users');
            $file_name = time() . '.' . $file->getClientOriginalExtension();
            $moved = $file->move($path, $file_name);
            $moved ? $user->image = $file_name : null;
        }

        $user->save();

        return back();

    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Xavier',
            'email' => 'xavi.pr2510@gmail.com',
            'password' => bcrypt('xavi123')
        ]);

        User::create([
            'name' => 'Toni',
            'email' => 'toni_142@icloud.com',
            'password' => bcrypt('toni123')
        ]);

        User::create([
            'name' => 'Cori',
            'email' => 'cori_142@icloud.com',
            'password' => bcrypt('cori123')
        ]);
    }
}

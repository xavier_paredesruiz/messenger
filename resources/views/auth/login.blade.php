@extends('layouts.app')

@section('content')
<b-container>
    <b-row align-h="center">
        <b-col cols="8" >
            <b-card title="Iniciar sesión">

                @if ($errors->any())
                    @foreach($errors->all() as $error)
                        <b-alert variant="danger" show>
                            {{ $error }}
                        </b-alert>
                    @endforeach
                @endif

                <b-form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf

                    <b-form-group id="emailGroup"
                                  label="Correo electrónico:"
                                  label-for="email">
                        <b-form-input id="email"
                                      type="email"
                                      name="email"
                                      required
                                      autofocus
                                      value="{{ old('email') }}"
                                      placeholder="p.e. example@email.com">
                        </b-form-input>
                    </b-form-group>

                    <b-form-group id="passwordGroup"
                                  label="Contraseña:"
                                  label-for="password">
                        <b-form-input id="password"
                                      type="password"
                                      name="password"
                                      required
                                      value="{{ old('password') }}">
                        </b-form-input>
                    </b-form-group>

                    <b-form-group>
                        <b-form-checkbox-group id="rememberCheck">
                            <b-form-checkbox 
                                    name="remember"
                                    {{ old('remember') ? 'checked="true"' : ''}}
                                    value="true">
                                
                                Recuérdame
                            </b-form-checkbox>
                        </b-form-checkbox-group>
                    </b-form-group>

                    <b-button type="submit" variant="primary">
                        Iniciar Sesión
                    </b-button>
                    <b-button href="{{ route('password.request') }}" variant="link">
                        Recuperar contraseña
                    </b-button>

                </b-form>
            </b-card>
        </b-col>
    </b-row>
</b-container>
@endsection

<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::create([
            'from_id' => 1,
            'to_id' => 2,
            'content' => "¡Hola! ¿Cómo estás?"
        ]);

        Message::create([
            'from_id' => 2,
            'to_id' => 1,
            'content' => "¡Genial! ¿Y tú?"
        ]);

        Message::create([
            'from_id' => 1,
            'to_id' => 3,
            'content' => "¡Buenos días! ¿Qué tal todo?"
        ]);

        Message::create([
            'from_id' => 3,
            'to_id' => 1,
            'content' => "¡Hey! Pues genial, ¿y tú?"
        ]);
    }
}
